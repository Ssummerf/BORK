package zork;

/**
 * Command for transforming multiple items into a different item
 * @author David, Scott, Ryan
 * @version Group Project 2
 */
public class CraftCommand extends Command {

	public CraftCommand(){
	}
	
        /**
         * Checks to see if the player has both the specified items
         * removes them from the player’s inventory and adds the crafted item if they are both present
         * notifies the player that they do not have the necessary items if they are not
         * @return String - affirmation that the items have been crafted
         * @return String - Notice that the player does not have the necessary items
         */

	@Override
	String execute() {
		return null;
	}

}
