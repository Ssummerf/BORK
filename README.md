# Bork Project

I worked on this project across a couple months at UMW. It served as an 
example for object oriented coding.

The overall idea for the project is a text based RPG that relies on a 
single synchronous game state across a dozen or so method files. It's heavily
related to the original game: https://en.wikipedia.org/wiki/Zork

I'd like to note that a portion of it was group work. My individual
responsibilities included taking on the lion's share of the code, debugging,
and implementing my partners untested code into the main program.

For additonal information regarding each file, please read their commented code.

## Command

An abstract executor

## CommandFactory

Parses an input for commands and acts appropriately. As the dialogue options
are specific (Ex. Make a pie), it needed to parse for a verb to act, and other
conditions to act on, as well as directional changes.

## DropCommand

Allows a user to drop an item into the current room, adding the item to the
room, and preforming an action regarding dropping certain items

## Dungeon

The main file that parses our premade Dungeon Text File, and reads it in to
create the room, item, and player locations. It also includes methods to load
a saved dungeon state.

## Event

Determines the type of event that'll take place on a players actions, 
and passes them to their individual method.

## Exit

Establishes a linked_list style of room connections

## GameState

Handles a players inventory, health, and various other player conditions

## Health Command

Modifies a players health, and returns unique status descriptions based
one certain values

## Interpreter

Handles introduction, file loading, and exit rules

## Item / Inventory Command

Creates an Item object that has a hashtable of messages, verbs, descriptions,
and events for an item. Read in from the text file, these are the bulk of
player interaction in the game.

## NPC

Similar to items in their creation and interaction, but with additional 
functions to make them NPC-esque

## Room

Parses the room portion of our Dungeon text file to read in NPCS,
items, and adjacent rooms to build our network of rooms

## Other *Commands are self explanatory

## Ending Comments

.bork files represent examples of the text we would parse in to establish
our "Dungeon". 

Due to the abstraction and specification of this project, it was a bit 
of a handful to create.
